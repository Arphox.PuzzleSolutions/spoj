﻿using ACPC10A;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class Tests
    {
        [TestCase("4 7 10", "AP 13")]
        [TestCase("2 6 18", "GP 54")]
        public void SampleTest(string input, string expectedOutput)
        {
            var solver = new Solver();
            var output = solver.Solve(input);
            Assert.That(output, Is.EqualTo(expectedOutput));
        }
    }
}