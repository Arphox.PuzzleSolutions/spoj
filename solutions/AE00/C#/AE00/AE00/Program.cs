﻿using System;

namespace AE00
{
    class Program
    {
        static void Main(string[] args)
        {
            int input = int.Parse(Console.ReadLine());
            var solver = new Solver();
            int result = solver.Solve(input);
            Console.WriteLine(result);
        }
    }

    public sealed class Solver
    {
        // See: https://oeis.org/A094820

        public int Solve(int input)
        {
            if (input == 1)
                return 1;

            int sum = 0;

            int highLimit = (int)Math.Sqrt(input);
            for (int divisor = 1; divisor <= highLimit; divisor++)
                if (input % divisor == 0)
                    sum++;

            sum += Solve(input - 1);

            return sum;
        }

        public int SolveDebugger(int input)
        {
            Print($"{input}:", ConsoleColor.DarkGreen);

            if (input == 1)
            {
                Print("1x1");
                return 1;
            }

            int sum = 0;

            int highLimit = (int)Math.Sqrt(input);
            for (int divisor = 1; divisor <= highLimit; divisor++)
            {
                if (input % divisor == 0)
                {
                    Print($"{divisor}x{input / divisor}");
                    sum++;
                }
            }

            int previous = Solve(input - 1);
            Print($"Sum of {input - 1} is {previous}", ConsoleColor.DarkGreen);

            sum += previous;
            return sum;
        }

        private static void Print(string message, ConsoleColor consoleColor = ConsoleColor.Gray)
        {
            Console.ForegroundColor = consoleColor;
            Console.WriteLine(message);
            Console.ResetColor();
        }
    }
}