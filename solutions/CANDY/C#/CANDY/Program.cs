﻿using System;
using System.Linq;

namespace CANDY
{
    class Program
    {
        static void Main()
        {
            var solver = new Solver();

            while (true)
            {
                int n = int.Parse(Console.ReadLine());
                if (n == -1)
                    return;

                int[] candies = new int[n];
                for (int i = 0; i < n; i++)
                    candies[i] = int.Parse(Console.ReadLine());

                int result = solver.Solve(candies);
                Console.WriteLine(result);
            }
        }
    }

    public sealed class Solver
    {
        public int Solve(int[] input)
        {
            int numberOfBags = input.Length;
            int sum = input.Sum();
            if (sum % numberOfBags != 0)
                return -1;

            int numberOfCandiesInEachBag = sum / numberOfBags;

            int moves = 0;
            for (int i = 0; i < numberOfBags; i++)
            {
                moves += Math.Abs(numberOfCandiesInEachBag - input[i]);
            }

            return moves / 2;
        }
    }
}