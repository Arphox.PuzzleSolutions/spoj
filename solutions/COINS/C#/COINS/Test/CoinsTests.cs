﻿using COINS;
using NUnit.Framework;

namespace Test
{
    [TestFixture]
    public sealed class CoinsTests
    {
        [TestCase(12, 13)]
        [TestCase(2, 2)]
        public void SampleTest(long input, long expectedResult)
        {
            var solver = new Solver();
            long result = solver.Solve(input);
            Assert.That(result, Is.EqualTo(expectedResult));
        }
    }
}