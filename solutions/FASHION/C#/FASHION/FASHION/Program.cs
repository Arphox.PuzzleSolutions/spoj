﻿using System;
using System.Linq;

namespace FASHION
{
    class Program
    {
        static void Main(string[] args)
        {
            var solver = new Solver();

            int t = int.Parse(Console.ReadLine());
            for (int i = 0; i < t; i++)
            {
                Console.ReadLine(); // discard
                int[] a = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
                int[] b = Console.ReadLine().Split(' ').Select(int.Parse).ToArray();
                int result = solver.Solve(a, b);
                Console.WriteLine(result);
            }
        }
    }

    public sealed class Solver
    {
        public int Solve(int[] a, int[] b)
        {
            Array.Sort(a);
            Array.Sort(b);

            int sum = 0;

            for (int i = 0; i < a.Length; i++)
                sum += a[i] * b[i];

            return sum;
        }
    }
}
