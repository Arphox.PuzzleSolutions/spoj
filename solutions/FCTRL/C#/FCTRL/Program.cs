﻿using System;

namespace FCTRL
{
    class Program
    {
        static void Main()
        {
            var solver = new Solver();

            int T = int.Parse(Console.ReadLine());
            for (int i = 0; i < T; i++)
            {
                int input = int.Parse(Console.ReadLine());
                int result = solver.Solve(input);
                Console.WriteLine(result);
            }
        }
    }

    public sealed class Solver
    {
        private static readonly int[] exponentsOfFive =
            { 5, 25, 125, 625, 3125, 15625, 78125, 390625, 1953125, 9765625, 48828125, 244140625, 1220703125, };

        public int Solve(int input)
        {
            int result = 0;

            for (int i = 0; i < exponentsOfFive.Length; i++)
                result += input / exponentsOfFive[i];

            return result;
        }
    }
}