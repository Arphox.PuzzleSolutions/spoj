﻿using FCTRL;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class SampleTests
    {
        [TestCase(3, 0)]
        [TestCase(60, 14)]
        [TestCase(100, 24)]
        [TestCase(1024, 253)]
        [TestCase(23456, 5861)]
        [TestCase(8735373, 2183837)]
        public void SampleTest(int input, int expectedOutput)
        {
            var solver = new Solver();

            int result = solver.Solve(input);

            Assert.That(result, Is.EqualTo(expectedOutput));
        }
    }
}