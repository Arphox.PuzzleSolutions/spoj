﻿using FCTRL2;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public sealed class FactorialTests
    {
        [TestCase(0, "1")]
        [TestCase(1, "1")]
        [TestCase(2, "2")]
        [TestCase(3, "6")]
        [TestCase(4, "24")]
        [TestCase(5, "120")]
        [TestCase(6, "720")]
        [TestCase(7, "5040")]
        [TestCase(8, "40320")]
        [TestCase(9, "362880")]
        [TestCase(10, "3628800")]
        [TestCase(11, "39916800")]
        [TestCase(12, "479001600")]
        [TestCase(13, "6227020800")]
        [TestCase(14, "87178291200")]
        [TestCase(15, "1307674368000")]
        [TestCase(16, "20922789888000")]
        [TestCase(17, "355687428096000")]
        [TestCase(18, "6402373705728000")]
        [TestCase(19, "121645100408832000")]
        [TestCase(20, "2432902008176640000")]
        [TestCase(21, "51090942171709440000")]
        [TestCase(22, "1124000727777607680000")]
        [TestCase(23, "25852016738884976640000")]
        [TestCase(24, "620448401733239439360000")]
        [TestCase(25, "15511210043330985984000000")]
        public void Until20(byte input, string expectedOutput)
        {
            var output = new Solver().Solve(input);
            Assert.That(output, Is.EqualTo(expectedOutput));
        }
    }
}