﻿using System;
using System.Numerics;

namespace JULKA
{
    class Program
    {
        static void Main()
        {
            BigInteger two = new BigInteger(2);

            for (int i = 0; i < 10; i++)
            {
                BigInteger together = BigInteger.Parse(Console.ReadLine());
                BigInteger diff = BigInteger.Parse(Console.ReadLine());

                BigInteger klaudia = (together + diff) / two;
                BigInteger natalia = klaudia - diff;
                Console.WriteLine(klaudia);
                Console.WriteLine(natalia);
            }
        }
    }
}