﻿The source limit is 700B so that is why this is ugly like that.
To submit, take the content of `Program.cs` and remove all leading whitespaces from all rows and it will be 698 (<700).

Or, here it is the shortest I could create (453B):

using System;using System.Linq;namespace L{class P{static void Main(){int t=int.Parse(Console.ReadLine());for(int c=0;c<t;c++){var i=Console.ReadLine().Split().Select(int.Parse).ToArray();Console.WriteLine(new O().S(i[0],i[1]));}}}class O{int[,] L=new int[,]{{0,0,0,0},{1,1,1,1},{4,8,6,2},{9,7,1,3},{6,4,6,4},{5,5,5,5},{6,6,6,6},{9,3,1,7},{4,2,6,8},{1,9,1,9}};public int S(int a,int b){if(a==0)return 0;if(a==1||b==0)return 1;return L[a%10,(b+2)%4];}}}