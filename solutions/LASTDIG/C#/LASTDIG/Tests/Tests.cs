﻿using LASTDIG;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public sealed class Tests
    {
        [TestCase(0, 10, 0)]
        [TestCase(9, 0, 1)]
        [TestCase(2, 3, 8)]
        [TestCase(2, 10, 4)]
        [TestCase(3, 10, 9)]
        [TestCase(6, 2, 6)]
        public void T(int a, int b, int expectedOutput)
        {
            var solver = new Solver();
            int output = solver.Solve(a, b);
            Assert.That(output, Is.EqualTo(expectedOutput));
        }
    }
}