﻿using System;

namespace NSTEPS
{
    class Program
    {
        static void Main()
        {
            int n = int.Parse(Console.ReadLine());
            var solver = new Solver();
            for (int i = 0; i < n; i++)
            {
                string input = Console.ReadLine();
                string output = solver.Solve(input);
                Console.WriteLine(output);
            }
        }
    }

    public sealed class Solver
    {
        public string Solve(string input)
        {
            string[] coords = input.Split(' ');
            int x = int.Parse(coords[0]);
            int y = int.Parse(coords[1]);

            if (y < 0 || !(y == x - 2 || y == x))
                return "No Number";

            int sum = x + y;

            if (x % 2 == 1)
                sum--;

            return sum.ToString();
        }
    }
}