﻿using NUnit.Framework;
using ONP;

namespace Tests
{
    [TestFixture]
    public sealed class OnpTests
    {
        [TestCase("(a+(b*c))", "abc*+")]
        [TestCase("((a+b)*(z+x))", "ab+zx+*")]
        [TestCase("((a+t)*((b+(a+c))^(c+d)))", "at+bac++cd+^*")]
        public void SampleTest(string input, string expectedOutput)
        {
            var output = new Solver().Solve(input);
            Assert.That(output, Is.EqualTo(expectedOutput));
        }
    }
}