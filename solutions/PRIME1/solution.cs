// Exported from "my submission history"
using System;
using System.Collections.Generic;

class Program
{
    private static void Main()
    {
        int numOfPairs = int.Parse(Console.ReadLine());
        for (int i = 0; i < numOfPairs; i++)
        {
            string[] bounds = Console.ReadLine().Split(' ');
            int lowerBound = int.Parse(bounds[0]);
            int upperBound = int.Parse(bounds[1]);
            List<int> primes = GetAllPrimesBetween_NOPARAMCHECK(lowerBound, upperBound);
            primes.ForEach(s => Console.WriteLine(s));
        }
    }
    private static List<int> GetAllPrimesBetween_NOPARAMCHECK(int lowerBound, int upperBound)
    {
        // NOTE: no need to check parameters since they are always given correctly
        List<int> primes = new List<int>(upperBound - lowerBound);

        for (int i = lowerBound; i <= upperBound; i ++)
        {
            if (IsPrime(i))
            {
                primes.Add(i);
            }
        }
        return primes;
    }
    private static bool IsPrime(int number)
    {
        if (number < 2)
        {
            return false;
        }
        else if (number == 2)
        {
            return true;
        }
        else if (number % 2 == 0)
        {
            return false;
        }

        for (int i = 3; i < Math.Sqrt(number) + 1; i += 2)
        {
            if (number % i == 0)
            {
                return false;
            }
        }
        return true;
    }
}