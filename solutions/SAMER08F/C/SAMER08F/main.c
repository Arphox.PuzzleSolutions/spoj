#include <stdio.h>

int main(void) {
    // Formula: `sum i^2, i=0 to n`
    // WolframAlpha link: https://www.wolframalpha.com/input/?i=sum+i%5E2,+i%3D0+to+n
    // Based on WA, the equivalent closed form is: `1/6n * (n + 1)(2n + 1)`

    int n = 1;
    while(n != 0){
        scanf("%d", &n);

        if (n != 0){
            printf("%d\n", n * (n + 1) * (2 * n + 1) / 6);
        }
    }

    return 0;
}
